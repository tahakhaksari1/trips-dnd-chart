import { time } from "console";
import { TripsChartTrip } from "./components/TripsDndChart/TripsData.type";

export const InitialData: TripsChartTrip[] = [
  {
    tripId: 1,
    courierId: 1,
    stops: [
      {
        stopId: "stop-1",
        estimatedWaitMinutes: 15,
        expectedTime: "09:00 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop1",
      },
      {
        stopId: "stop-2",
        estimatedWaitMinutes: 10,
        expectedTime: "10:15 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop2",
      },
      {
        stopId: "stop-3",
        estimatedWaitMinutes: 15,
        expectedTime: "09:00 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop1",
      },
      {
        stopId: "stop-4",
        estimatedWaitMinutes: 15,
        expectedTime: "09:00 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop1",
      },
      {
        stopId: "stop-5",
        estimatedWaitMinutes: 15,
        expectedTime: "08:00 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop1",
      },
      {
        stopId: "stop-6",
        estimatedWaitMinutes: 15,
        expectedTime: "09:00 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop6",
      },
    ],
  },
  {
    tripId: 2,
    courierId: 2,
    stops: [
      {
        stopId: "stop-1",
        estimatedWaitMinutes: 15,
        expectedTime: "08:00 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop1",
      },
      {
        stopId: "stop-2",
        estimatedWaitMinutes: 10,
        expectedTime: "10:15 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop2",
      },
    ],
  },
  {
    tripId: 2,
    courierId: 2,
    stops: [
      {
        stopId: "stop-1",
        estimatedWaitMinutes: 15,
        expectedTime: "06:00 pm",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop1",
      },
      {
        stopId: "stop-2",
        estimatedWaitMinutes: 10,
        expectedTime: "10:15 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop2",
      },
    ],
  },
  {
    tripId: 2,
    courierId: 2,
    stops: [
      {
        stopId: "stop-1",
        estimatedWaitMinutes: 15,
        expectedTime: "09:00 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop1",
      },
      {
        stopId: "stop-2",
        estimatedWaitMinutes: 10,
        expectedTime: "10:15 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop2",
      },
    ],
  },
  {
    tripId: 2,
    courierId: 2,
    stops: [
      {
        stopId: "stop-1",
        estimatedWaitMinutes: 15,
        expectedTime: "09:00 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop1",
      },
      {
        stopId: "stop-2",
        estimatedWaitMinutes: 10,
        expectedTime: "10:15 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop2",
      },
    ],
  },
  {
    tripId: 2,
    courierId: 2,
    stops: [
      {
        stopId: "stop-1",
        estimatedWaitMinutes: 15,
        expectedTime: "09:00 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop1",
      },
      {
        stopId: "stop-2",
        estimatedWaitMinutes: 10,
        expectedTime: "10:15 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop2",
      },
    ],
  },
  {
    tripId: 2,
    courierId: 2,
    stops: [
      {
        stopId: "stop-1",
        estimatedWaitMinutes: 15,
        expectedTime: "09:00 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop1",
      },
      {
        stopId: "stop-2",
        estimatedWaitMinutes: 10,
        expectedTime: "10:15 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop2",
      },
    ],
  },
  {
    tripId: 2,
    courierId: 2,
    stops: [
      {
        stopId: "stop-1",
        estimatedWaitMinutes: 15,
        expectedTime: "09:00 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop1",
      },
      {
        stopId: "stop-2",
        estimatedWaitMinutes: 10,
        expectedTime: "10:15 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop2",
      },
    ],
  },
  {
    tripId: 2,
    courierId: 2,
    stops: [
      {
        stopId: "stop-1",
        estimatedWaitMinutes: 15,
        expectedTime: "09:00 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop1",
      },
      {
        stopId: "stop-2",
        estimatedWaitMinutes: 10,
        expectedTime: "10:15 am",
        location: {
          lan: 50.12345,
          lat: 39.6548,
        },
        title: "Stop2",
      },
    ],
  },
];
