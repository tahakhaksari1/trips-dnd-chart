import { CSSProperties } from "react";
import { TripsChartColorPalette } from "../color-palette";
import { getTimeArray } from "../common";
import { TripsChartTrip } from "../TripsData.type";
import TripRow from "./TripRow";

export default function TimeLine(props: {
  trips: TripsChartTrip[];
  notificationAreaWidth: number;
  curiersAreaWidth: number;
  rowsHeight: number;
  height: number;
  timeStart: string;
  timeEnd: string;
  timeMinor: number;
  timeMinorPixels: number;
  palette: TripsChartColorPalette;
}) {
  const container: CSSProperties = {
    display: "block",
    whiteSpace: "nowrap",
    width: "100%",
    paddingLeft: props.curiersAreaWidth,
  };

  const timeGuide: CSSProperties = {
    position: "absolute",
    top: 0,
    width: props.timeMinorPixels,
    height: props.trips.length * (props.rowsHeight + 8),
    backgroundColor: "rgba(64,200,64,0.05)",
  };
  const timeArray = getTimeArray(
    props.timeStart,
    props.timeEnd,
    props.timeMinor
  );
  const startTime = timeArray[0].time;
  return (
    <div style={container}>
      {timeArray.map((time, index) => (
        <div
          key={`timeGuide-${index}`}
          style={{
            ...timeGuide,
            left:
              props.notificationAreaWidth +
              props.curiersAreaWidth +
              index * props.timeMinorPixels,
            backgroundColor:
              index % 2 ? "none" : timeGuide.backgroundColor ?? "none",
          }}
        ></div>
      ))}

      {props.trips.map((trip) => {
        return (
          <TripRow
            trip={trip}
            height={props.rowsHeight}
            palette={props.palette}
            notificationAreaWidth={props.notificationAreaWidth}
            timeStart={startTime}
            timeMinor={props.timeMinor}
            timeMinorPixels={props.timeMinorPixels}
          />
        );
      })}
    </div>
  );
}
