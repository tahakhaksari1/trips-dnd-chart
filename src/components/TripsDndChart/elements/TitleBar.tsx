import { CSSProperties } from "styled-components";
import {
  TripsChartColorPalette,
  TripsChartDefaultPalette,
} from "../color-palette";

export default function TitleBar(props: {
  palette?: TripsChartColorPalette;
  height: number;
}) {
  const palette = props.palette ?? TripsChartDefaultPalette;
  const TitleBarStyle: CSSProperties = {
    backgroundColor: palette.titleBarBackground,
    color: palette.titleBarText,
    padding: 8,
    boxSizing: "border-box",
    height: props.height,
  };
  return <div style={TitleBarStyle}>Trips</div>;
}
