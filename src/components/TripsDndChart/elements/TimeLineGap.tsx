import { CSSProperties } from "styled-components";
import {
  TripsChartColorPalette,
  TripsChartDefaultPalette,
} from "../color-palette";
export default function TimeLineGap(props: {
  palette?: TripsChartColorPalette;
  height: number;
  width: number;
}) {
  const palette = props.palette ?? TripsChartDefaultPalette;
  const style: CSSProperties = {
    position: "sticky",
    top: 0,
    height: props.height,
    width: props.width,
    backgroundColor: palette.timelineBackground,
  };
  return <div style={style}></div>;
}
