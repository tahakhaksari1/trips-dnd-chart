import moment from "moment";
import { CSSProperties } from "styled-components";
import { TripsChartColorPalette } from "../color-palette";
import { getTimeArray } from "../common";

export default function TimeBand(props: {
  start: string;
  end: string;
  minor: number;
  minorPixels: number;
  scroll: number;
  left: number;
  height: number;
  paddingLeft: number;
  pallete: TripsChartColorPalette;
}) {
  const containerStyle: CSSProperties = {
    display: "inline",
    position: "absolute",
    overflow: "hidden",
    whiteSpace: "nowrap",
    backgroundColor: props.pallete.timelineBackground,
    top: 0,
    left: 0,
    right: 0,
    height: props.height,
  };
  const bandStyle: CSSProperties = {
    display: "block",
    position: "relative",
    backgroundColor: props.pallete.timelineBackground,
    top: 0,
    left: -props.scroll,
    height: props.height,
    whiteSpace: "nowrap",
    paddingLeft: props.left,
  };
  const timeArray = getTimeArray(props.start, props.end, props.minor);
  const today = moment().format("YYYY-MMM-DD ");
  return (
    <div style={containerStyle}>
      <div style={bandStyle}>
        <span
          style={{
            display: "inline-block",
            width: props.paddingLeft,
            height: props.height,
          }}
        >
          &nbsp;
        </span>
        <div
          style={{
            display: "inline-block",
            whiteSpace: "nowrap",
          }}
        >
          {timeArray.map((time, index) => (
            <div
              key={index}
              style={{
                display: "inline-block",
                width: `${props.minorPixels}px`,
              }}
            >
              {time.isMajor
                ? moment(today + time.time, "YYYY-MMM-DD hh:mm a").format("h")
                : " "}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
