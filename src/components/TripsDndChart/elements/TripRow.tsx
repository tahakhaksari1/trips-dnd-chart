import moment from "moment";
import { CSSProperties } from "styled-components";
import {
  TripsChartColorPalette,
  TripsChartDefaultPalette,
} from "../color-palette";
import { MinutesToPixels } from "../common";
import { TripsChartStop, TripsChartTrip } from "../TripsData.type";
import "./styles.sass";

const Spacer = (props: { color?: string; width: number }) => {
  return (
    <div
      className={`spacer${props.color ? " color" : ""}`}
      style={{ width: props.width }}></div>
  );
};
const Finish = (props: { color?: string }) => {
  return (
    <div className={`finish${props.color ? ` color ${props.color}` : ""}`}>
      x
    </div>
  );
};
const Stop = (props: {
  stop: TripsChartStop;
  index: number;
  timeMinor: number;
  timeMinorPixels: number;
}) => {
  return (
    <div className="stop">
      <span
        className="title"
        style={{
          width: MinutesToPixels(
            props.stop.estimatedWaitMinutes,
            props.timeMinor,
            props.timeMinorPixels
          ),
        }}>
        {props.index + 1}
      </span>
    </div>
  );
};
export default function TripRow(props: {
  trip: TripsChartTrip;
  palette?: TripsChartColorPalette;
  height: number;
  notificationAreaWidth: number;
  timeStart: string;
  timeMinor: number;
  timeMinorPixels: number;
  color?: string;
}) {
  const row: CSSProperties = {
    display: "block",
    whiteSpace: "nowrap",
    height: props.height,
    boxSizing: "border-box",
    marginTop: 8,
    paddingLeft: props.notificationAreaWidth,
  };
  const calculateRouteTimeAfterStopByIndex = (index: number): number => {
    const currentTime = moment(
      props.trip.stops[index].expectedTime,
      "hh:mm a"
    ).add(props.trip.stops[index].estimatedWaitMinutes, "minutes");
    if (index < props.trip.stops.length - 1) {
      const nextStopTime = moment(
        props.trip.stops[index + 1].expectedTime,
        "hh:mm a"
      );
      return nextStopTime.diff(currentTime, "minutes");
    }
    return 0;
  };
  const startPx = MinutesToPixels(
    moment(props.trip.stops[0]?.expectedTime, "hh:mm a").diff(
      moment(props.timeStart, "hh:mm a"),
      "minutes"
    ),
    props.timeMinor,
    props.timeMinorPixels
  );

  return (
    <div style={row} className="trip-row">
      <Spacer width={startPx} />
      {props.trip.stops?.map((stop, index) => (
        <>
          <Stop
            stop={stop}
            index={index}
            timeMinor={props.timeMinor}
            timeMinorPixels={props.timeMinorPixels}
          />
          <Spacer
            width={MinutesToPixels(
              calculateRouteTimeAfterStopByIndex(index),
              props.timeMinor,
              props.timeMinorPixels
            )}
            color={"blue"}
          />
        </>
      ))}
      <Finish />
    </div>
  );
}
