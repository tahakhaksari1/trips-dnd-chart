import moment from "moment";
import { CSSProperties } from "styled-components";
import { TripsChartColorPalette } from "../color-palette";
import { TripsChartTrip } from "../TripsData.type";

export default function TripsCouriers(props: {
  trips: TripsChartTrip[];
  palette: TripsChartColorPalette;
  scroll: number;
  width: number;
  rowHeight: number;
}) {
  const containerStyle: CSSProperties = {
    display: "inline-block",
    position: "absolute",
    top: 0,
    left: 0,
    width: props.width,
  };
  const bandStyle: CSSProperties = {
    display: "inline-block",
    position: "absolute",
    top: 0,
    // left: props.scroll,
    width: props.width,
  };
  const rowStyle: CSSProperties = {
    display: "block",
    height: props.rowHeight + 8,
    padding: "16px 8px 8px 8px",
    boxSizing: "border-box",
    backgroundColor: props.palette.couriersBackground,
  };
  const iconStyle: CSSProperties = {
    display: "inline-block",
    height: props.rowHeight - 8,
    width: props.rowHeight - 8,
    margin: "-4px 4px 0 4px",
    borderRadius: "50%",
    backgroundColor: "rgba(200,255,200,0.4)",
  };
  return (
    <div style={containerStyle}>
      <div style={bandStyle}>
        {props.trips.map((trip) => (
          <div style={rowStyle}>
            <div style={iconStyle}></div>
            {trip.courierId}
          </div>
        ))}
      </div>
    </div>
  );
}
