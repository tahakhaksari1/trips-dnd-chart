export interface TripsChartColorPalette {
  titleBarBackground: string;
  titleBarText: string;
  borders: string;
  timelineBackground: string;
  couriersBackground: string;
}
export const TripsChartDefaultPalette: TripsChartColorPalette = {
  titleBarBackground: "#7FC15E",
  titleBarText: "#D0E7C2",
  borders: "#D0E7C2",
  timelineBackground: "#D0E7C2",
  couriersBackground: "white",
};
