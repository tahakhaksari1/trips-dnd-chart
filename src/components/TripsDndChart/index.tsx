import { TripsChartTrip } from "./TripsData.type";
import {
  TripsChartDefaultPalette,
  TripsChartColorPalette,
} from "./color-palette";
import { useCallback, useEffect, useState } from "react";
import moment from "moment";
import TitleBar from "./elements/TitleBar";
import TimeLineGap from "./elements/TimeLineGap";
import TimeBand from "./elements/TimeBand";
import TripsCouriers from "./elements/TripsCouriers";
import TimeLine from "./elements/TimeLine";

const TripsDndChart = (props: {
  trips: TripsChartTrip[];
  palette?: TripsChartColorPalette;
  style?: any;
}) => {
  const [palette, setPalette] = useState(
    props.palette ?? TripsChartDefaultPalette
  );
  const [courierAreaWith, setCourierAreaWidth] = useState(150);
  const [timeLineMinor, setTimelineMinor] = useState(5);
  const [timeLineMinorPx, setTimelineMinorPx] = useState(10);
  const [timeLineStart, setTimeLineStart] = useState("08:23 am");
  const [timeLineEnd, setTimeLineEnd] = useState("09:00 pm");
  const [timeLineScroll, setTimeLineScroll] = useState({ left: 0, top: 0 });
  const timeLineHeight = 24;
  const rowsHeight = 64;
  const titleAreaHeight = 32;
  const notificationAreaWidth = 32;

  const TimeLineGrids = (
    startTime: string,
    endTime: string,
    minorMinutes: number,
    minorPx: number
  ) => {
    const today = moment().format("YYYY-MM-DD ");
    let start = moment(today + startTime).startOf("hour");
    let end = moment(today + endTime)
      .add(1, "hours")
      .startOf("hour");
    const timeArray: { time: string; isMajor: boolean }[] = [];
    let prevHour = 0;
    for (let i = start; i <= end; i = i.add(minorMinutes, `minutes`)) {
      let isMajor = false;
      if (i.hours() !== prevHour) {
        isMajor = true;
        prevHour = i.hours();
      }
      timeArray.push({ time: i.format("hh:mm a"), isMajor });
    }
    return (
      <div>
        <div>
          {timeArray.map((time) => (
            <div style={{ display: "inline-block", width: `${minorPx}px` }}>
              {time.isMajor ? moment(today + time.time).format("h a") : "."}
            </div>
          ))}
        </div>
      </div>
    );
  };

  const timeLineScrollHandler = (e: React.UIEvent<HTMLDivElement>) => {
    if (e.currentTarget)
      setTimeLineScroll({
        left: e.currentTarget.scrollLeft,
        top: e.currentTarget.scrollTop,
      });
  };
  return (
    <div
      style={{
        ...(props.style ?? {}),
        border: `1px solid ${palette.titleBarBackground}`,
      }}
    >
      <div>
        <TitleBar palette={palette} height={titleAreaHeight} />
        <div style={{ position: "relative" }}>
          <TimeBand
            start={timeLineStart}
            end={timeLineEnd}
            minor={timeLineMinor}
            minorPixels={timeLineMinorPx}
            scroll={timeLineScroll.left}
            height={timeLineHeight}
            left={courierAreaWith}
            paddingLeft={notificationAreaWidth}
            pallete={palette}
          />
        </div>
      </div>
      <div
        style={{
          display: "flex",
          position: "relative",
          top: timeLineHeight,
          height: `calc(100% - ${titleAreaHeight + timeLineHeight}px)`,
        }}
      >
        <div
          onScroll={timeLineScrollHandler}
          style={{
            position: "absolute",
            overflow: "auto",
            width: "100%",
            top: 0,
            height: "100%",
          }}
        >
          <TripsCouriers
            trips={props.trips}
            palette={palette}
            scroll={timeLineScroll.left}
            width={courierAreaWith}
            rowHeight={rowsHeight}
          />
          <TimeLine
            trips={props.trips}
            notificationAreaWidth={notificationAreaWidth}
            rowsHeight={rowsHeight}
            curiersAreaWidth={courierAreaWith}
            height={titleAreaHeight + timeLineHeight}
            timeStart={timeLineStart}
            timeEnd={timeLineEnd}
            timeMinor={timeLineMinor}
            timeMinorPixels={timeLineMinorPx}
            palette={palette}
          />
        </div>
      </div>
    </div>
  );
};

export { TripsDndChart };
