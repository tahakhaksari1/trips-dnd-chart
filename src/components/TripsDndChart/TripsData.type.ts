export interface TripsChartStop {
  stopId: number | string;
  location: { lan: number | string; lat: number | string };
  title: string;
  estimatedWaitMinutes: number;
  expectedTime: string;
  occurrenceTime?: string;
}
export interface TripsChartTrip {
  tripId: number | string;
  courierId: number | string;
  stops: TripsChartStop[];
}
