import moment from "moment";

export function MinutesToPixels(
  minutes: number,
  timeMinor: number,
  timeMinorPixels: number
) {
  return (minutes / timeMinor) * timeMinorPixels;
}

export function getTimeArray(
  timeStart: string,
  timeEnd: string,
  minor: number
) {
  const today = moment().format("YYYY-MMM-DD ");
  let start = moment(today + timeStart, "YYYY-MMM-DD hh:mm a").startOf("hour");
  let end = moment(today + timeEnd, "YYYY-MMM-DD hh:mm a")
    .add(1, "hours")
    .startOf("hour");
  const timeArray: { time: string; isMajor: boolean }[] = [];
  let prevHour = 0;

  for (let i = start; i <= end; i = i.add(minor, `minutes`)) {
    let isMajor = false;
    if (i.hours() !== prevHour) {
      isMajor = true;
      prevHour = i.hours();
    }
    timeArray.push({ time: i.format("hh:mm a"), isMajor });
  }
  return timeArray;
}
