import React, { useState } from "react";
import { InitialData } from "./initial-data";
import { TripsDndChart } from "./components/TripsDndChart";

function App() {
  const [tripdData, setTripsData] = useState(InitialData);
  return (
    <div className="App">
      <TripsDndChart style={{ height: 400, width: 900 }} trips={tripdData} />
    </div>
  );
}

export default App;
